<?php
namespace Src\Controller;

use Src\TableGateways\RunnerGateway;

class RunnerController {

    private $db;
    private $requestMethod;
    private $runnerId;

    private $runnerGateway;

    public function __construct($db, $requestMethod, $runnerId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->runnerId = $runnerId;

        $this->runnerGateway = new RunnerGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->runnerId) {
                    $response = $this->getRunner($this->runnerId);
                } else {
                    $response = $this->getAllRunners();
                };
                break;
            case 'POST':
                $response = $this->createRunnerFromRequest();
                break;
            case 'PUT':
                $response = $this->updateRunnerFromRequest($this->runnerId);
                break;
            case 'DELETE':
                $response = $this->deleteRunner($this->runnerId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllRunners()
    {
        $result = $this->runnerGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getRunner($id)
    {
        $result = $this->runnerGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createRunnerFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateRunner($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->runnerGateway->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateRunnerFromRequest($id)
    {
        $result = $this->runnerGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateRunner($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->runnerGateway->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteRunner($id)
    {
        $result = $this->runnerGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->runnerGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateRunner($input)
    {
        if (! isset($input['firstname'])) {
            return false;
        }
        if (! isset($input['lastname'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
