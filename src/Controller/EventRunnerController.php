<?php
namespace Src\Controller;

use Src\TableGateways\EventRunnerGateway;

class EventRunnerController {

    private $db;
    private $requestMethod;
    private $eventrunnerId;

    private $eventrunnerGateway;

    public function __construct($db, $requestMethod, $eventrunnerId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->eventrunnerId = $eventrunnerId;

        $this->eventrunnerGateway = new EventRunnerGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->eventrunnerId) {
                    $response = $this->getEventRunner($this->eventrunnerId);
                } else {
                    $response = $this->getAllEventRunners();
                };
                break;
            case 'POST':
                $response = $this->createEventRunnerFromRequest();
                break;
            case 'PUT':
                $response = $this->updateEventRunnerFromRequest($this->eventrunnerId);
                break;
            case 'DELETE':
                $response = $this->deleteEventRunner($this->eventrunnerId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllEventRunners()
    {
        $result = $this->eventrunnerGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getEventRunner($id)
    {
        $result = $this->eventrunnerGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createEventRunnerFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateEventRunner($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->eventrunnerGateway->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateEventRunnerFromRequest($id)
    {
        $result = $this->eventrunnerGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateEventRunner($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->eventrunnerGateway->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteEventRunner($id)
    {
        $result = $this->eventrunnerGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->eventrunnerGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateEventRunner($input)
    {
        if (! isset($input['firstname'])) {
            return false;
        }
        if (! isset($input['lastname'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
