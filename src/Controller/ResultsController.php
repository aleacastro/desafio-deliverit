<?php
namespace Src\Controller;

use Src\TableGateways\ResultGateway;

class ResultController {

    private $db;
    private $requestMethod;
    private $resultId;

    private $resultGateway;

    public function __construct($db, $requestMethod, $resultId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->resultId = $resultId;

        $this->resultGateway = new ResultGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->resultId) {
                    $response = $this->getResult($this->resultId);
                } else {
                    $response = $this->getAllResults();
                };
                break;
            case 'POST':
                $response = $this->createResultFromRequest();
                break;
            case 'PUT':
                $response = $this->updateResultFromRequest($this->resultId);
                break;
            case 'DELETE':
                $response = $this->deleteResult($this->resultId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllResults()
    {
        $result = $this->resultGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getResult($id)
    {
        $result = $this->resultGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createResultFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateResult($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->resultGateway->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateResultFromRequest($id)
    {
        $result = $this->resultGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateResult($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->resultGateway->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteResult($id)
    {
        $result = $this->resultGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->resultGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateResult($input)
    {
        if (! isset($input['firstname'])) {
            return false;
        }
        if (! isset($input['lastname'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
