<?php
namespace Src\TableGateways;

class ResultGateway {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT
                *
            FROM
                results;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($event_id, $runner_id)
    {
        $statement = "
            SELECT
                *
            FROM
                results
            WHERE event_id = ? and runner_id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "
            INSERT INTO results
                (event_id, runner_id)
            VALUES
                (:event_id, :runner_id);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => $input['event_id'],
                'runner_id'  => $input['runner_id']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($event_id, $runner_id, Array $input)
    {
        $statement = "
            UPDATE results
            SET
                event_id = :event_id,
                runner_id  = :runner_id
            WHERE event_id = :event_id and runner_id = runner_id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => (int) $event_id,
                'runner_id' => (int) $runner_id
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($event_id, $runner_id)
    {
        $statement = "
            DELETE FROM result
            WHERE event_id = :event_id and runner_id = :runner_id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => (int) $event_id,
                'runner_id' => (int) $runner_id
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}