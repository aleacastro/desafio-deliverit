<?php
namespace Src\TableGateways;

class EventGateway {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT
                *
            FROM
                event;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT
                *
            FROM
                events
            WHERE id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "
            INSERT INTO events
                (type, event_date)
            VALUES
                (:type, :event_date);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'type' => $input['type'],
                'event_date'  => $input['event_date']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, Array $input)
    {
        $statement = "
            UPDATE events
            SET
                type = :type,
                event_date  = :event_date
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'type' => $input['type'],
                'event_date'  => $input['event_date']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "
            DELETE FROM events
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function classification ($id) {
        $statement = "
            SET @row_number = 0; 
            SELECT
                events.id,
                events.type,
                runners.id,
                YEAR(CURDATE()) - YEAR(runners.birth_date) AS age,
                runners.name,
                (@row_number:=@row_number + 1) AS position,
                CONVERT(TIMEDIFF(results.end_time, results.start_time),CHAR(100)) AS final_time
            FROM
                results
                inner join events on events.id = results.event_id
                inner join runners on runners.id = results.runner_id
            WHERE results.event_id = ?
            ORDER BY 7;
        
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function classificationAge ($id) {
        $statement = "
        SET @row_number = 0; 
        SELECT
            events.id,
            events.type,
            runners.id,
            YEAR(CURDATE()) - YEAR(runners.birth_date) AS age,
            runners.name,
            (@row_number:=@row_number + 1) AS position,
            CONVERT(TIMEDIFF(results.end_time, results.start_time),CHAR(100)) AS final_time,
            (CASE
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 25 THEN '18 – 25 anos'
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 35 THEN '25 – 35 anos'
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 45 THEN '35 – 45 anos'
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 55 THEN '45 – 55 anos'
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) > 55 THEN 'Acima de 55 anos'
            END) category
        FROM
            results
            inner join events on events.id = results.event_id
            inner join runners on runners.id = results.runner_id
        WHERE results.event_id = 1
        order by 
            (CASE
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 25 THEN 1
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 35 THEN 2
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 45 THEN 3
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) <= 55 THEN 4
                WHEN YEAR(CURDATE()) - YEAR(runners.birth_date) > 55 THEN 5
            END), 7;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }    
}