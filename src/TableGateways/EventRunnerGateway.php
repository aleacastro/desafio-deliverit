<?php
namespace Src\TableGateways;

class EventRunnerGateway {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT
                *
            FROM
                events_runners;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($event_id, $runner_id)
    {
        $statement = "
            SELECT
                *
            FROM
                events_runners
            WHERE event_id = ? and runner_id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {

        $statement = "
            SELECT
                event_date
            FROM
                events
            WHERE id = :event_id ;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => $input['event_id'],
            ));
            $resultEvent = $statement->fetchAll(\PDO::FETCH_ASSOC);

        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        if (count($resultEvent)) {
        
            $statement = "
                SELECT
                    count(events_runners.id)
                FROM
                    events_runners 
                    inner join runners on events_runners.runner_id 
                    inner join events on events_runners.event_id 
                WHERE events_runners.event_id = :event_id and runner_id = :runner_id and
                    events.event_date = :event_date;
            ";

            try {
                $statement = $this->db->prepare($statement);
                $statement->execute(array(
                    'event_id' => $input['event_id'],
                    'runner_id'  => $input['runner_id'],
                    'event_date'  => $input['event_date'],
                ));
                $resultEvent_Runner =  $statement->fetchAll(\PDO::FETCH_ASSOC);

                if ( count($resultEvent_Runner) ) {
                    exit('Já existe cadastro de outro evento com esse corredor nessa data');
                }

            } catch (\PDOException $e) {
                exit($e->getMessage());
            }
        }

        $statement = "
            INSERT INTO events_runners
                (event_id, runner_id)
            VALUES
                (:event_id, :runner_id);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => $input['event_id'],
                'runner_id'  => $input['runner_id']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($event_id, $runner_id, Array $input)
    {
        $statement = "
            UPDATE events_runners
            SET
                event_id = :event_id,
                runner_id  = :runner_id
            WHERE event_id = :event_id and runner_id = runner_id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => (int) $event_id,
                'runner_id' => (int) $runner_id
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($event_id, $runner_id)
    {
        $statement = "
            DELETE FROM events_runners
            WHERE event_id = :event_id and runner_id = :runner_id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'event_id' => (int) $event_id,
                'runner_id' => (int) $runner_id
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}