DROP TABLE IF EXISTS `runners`;

CREATE TABLE `runners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_card` varchar(11) NOT NULL,
  `birth_date` date NOT NULL,
  UNIQUE KEY unique_id_card (id_card),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `runners` (name, id_card, birth_date) VALUES ('Alessandro','77670400110', '1974-08-30'),('Rodrigo','11111111111', '1979-02-10'),('Jean','2222222222', '1995-01-01');


DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint unsigned NOT NULL,
  `event_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `events` (`type`, `event_date`) VALUES (5, '2021-12-31 10:00:00'),(10, '2022-01-01 09:00:00');

DROP TABLE IF EXISTS `events_runners`;

CREATE TABLE `events_runners` (
  `event_id` bigint(20) unsigned NOT NULL,
  `runner_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`event_id`, `runner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `events_runners` (event_id, runner_id)  VALUES (1, 1),(2, 2);

DROP TABLE IF EXISTS `results`;

CREATE TABLE `results` (
  `event_id` bigint(20) unsigned NOT NULL,
  `runner_id` bigint(20) unsigned NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`event_id`, `runner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `results` (event_id, runner_id, start_time, end_time) VALUES (1, 1, '08:00:00', '11:00:00'),(2, 2, '14:00:00', '16:00:00');
